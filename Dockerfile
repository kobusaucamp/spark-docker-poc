FROM openjdk:8-jre-alpine

RUN apk --update add git curl tar bash ncurses nmap && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*

WORKDIR /app

#ADD target/scala-**/test_2.11-0.1.0-SNAPSHOT.jar /app/app.jar
ADD target/scala-**/test-assembly-0.1.0-SNAPSHOT.jar /app/app.jar

ENTRYPOINT ["java","-jar","/app/app.jar"]
