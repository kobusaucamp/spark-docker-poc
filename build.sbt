ThisBuild / scalaVersion     := "2.11.12"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.gertyes.temp"
ThisBuild / organizationName := "temp"

resolvers += Resolver.url("bintray-sbt-plugins", url("http://dl.bintray.com/sbt/sbt-plugin-releases"))(Resolver.ivyStylePatterns)

lazy val root = (project in file("."))
  .settings(
    name := "test",
    mainClass in Compile := Some("com.gertyes.temp.MinTemperatures")
  )

libraryDependencies ++= Seq(
  // compile  - when running local
  // provided - when packaging with assembly
  "org.apache.spark" %% "spark-core" % "2.4.0" % "compile",
  "org.apache.spark" %% "spark-sql" % "2.4.0" % "compile",
  "org.apache.spark" %% "spark-graphx" % "2.4.0" % "compile",
  "org.apache.hadoop" % "hadoop-hdfs" % "2.7.0" % "compile",
  "org.apache.hadoop" % "hadoop-client" % "2.7.0" % "compile"
)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)
