package com.gertyes.temp

import java.io.{File, PrintWriter}

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, LocalFileSystem, Path}
import org.apache.hadoop.hdfs.DistributedFileSystem
import org.apache.spark.sql.SparkSession

import scala.math.min
import scala.io.Source

/** Find the minimum temperature by weather station */
object MinTemperatures {

  def parseLine(line:String)= {
    val fields = line.split(",")
    val stationID = fields(0)
    val entryType = fields(2)
    val temperature = fields(3).toFloat * 0.1f * (9.0f / 5.0f) + 32.0f
    (stationID, entryType, temperature)
  }
    /** Our main function where the action happens */
  def main(args: Array[String]) {

    val spark = SparkSession.builder()
      .appName("MinTemperatures")
//      .master("spark://127.0.0.1:7077")
      .master("spark://spark-master:7077")
      .config("deploy-mode", "client")
//      .config("spark.jars", "target/scala-2.11/test-assembly-0.1.0-SNAPSHOT.jar")
      .config("spark.jars", "/app/app.jar")
      .getOrCreate()

    val hadoopConfig = spark.sparkContext.hadoopConfiguration
    hadoopConfig.set("fs.hdfs.impl", classOf[DistributedFileSystem].getName)
    hadoopConfig.set("fs.file.impl", classOf[LocalFileSystem].getName)

    // Read each line of input data
//    val lines = spark.sparkContext.textFile("/app/resources/1800.csv")
    val lines = spark.sparkContext.textFile("hdfs://172.33.145.72:9000/user/root/test/1800.csv")

    // Convert to (stationID, entryType, temperature) tuples
    val parsedLines = lines.map(parseLine)

    // Filter out all but TMIN entries
    val minTemps = parsedLines.filter(x => x._2 == "TMIN")

    // Convert to (stationID, temperature)
    // Converts to key value RDD
    val stationTemps = minTemps.map(x => (x._1, x._3.toFloat))

    // Reduce by stationID retaining the minimum temperature found
    val minTempsByStation = stationTemps.reduceByKey( (x,y) => min(x,y))

    // Collect, format, and print the results
    val results = minTempsByStation.collect()

    val writer = new PrintWriter(new File("test.txt"))

    for (result <- results.sorted) {
       val station = result._1
       val temp = result._2
       val formattedTemp = f"$temp%.2f F"
       println(s"$station minimum temperature: $formattedTemp")

       writer.write(s"$station minimum temperature: $formattedTemp/n")

    }
    writer.close()

    val bufferedSource = Source.fromFile("test.txt")
    for (line <- bufferedSource.getLines) {
      println(line.toUpperCase)
    Hdfs.write("hdfs://172.33.145.72:9000", "test/output.txt", line.getBytes)
    }
    bufferedSource.close

//    spark.sparkContext.parallelize(List(1,2,3)).map(x=>List(x,x,x)).collect
//    val test = spark.sparkContext.parallelize(List(1,2,3)).map(x=>List(x,x,x)).collect
//    test.foreach(x => println(x))
  }

  object Hdfs extends App {

    def write(uri: String, filePath: String, data: Array[Byte]) = {
      System.setProperty("HADOOP_USER_NAME", "gert")
      val path = new Path(filePath)
      val conf = new Configuration()
      conf.set("fs.defaultFS", uri)
      val fs = FileSystem.get(conf)
      val os = fs.create(path)
      os.write(data)
      fs.close()
    }
  }
}