# Spark Scala template

The POC is just a basic scala program which makes use of the spark framework. The app reads from and writes to a HDFS instance running on a remote location.

# Get your spark cluster running

1. This is in the docker-spark project in gitlab...
2. Run ```docker networks ls``` to get the network for the spark cluster. We need to feed this into the docker run command in the next phase. (It seems to be `dockerspark_default`)

# Getting the docker container to run

1. Run ```sbt clean assembly``` from the project root.
2. Run ```docker build -t <your-image-name>```.
3. Run ```docker images``` to get the image tag. (Should be latest if it wasn't specified)
3. Run ```docker run --network=dockerspark_default -it <your-image-name>:latest``` 
 